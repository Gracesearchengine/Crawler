This project is the crawler step in the core of the whole search engine. the project 
mainly takes two actions towards searching and crawling the url as well as the content 
along with it. first step of the action is to search for the url, validate and plug to 
the crawler which then parses and indexes the retrieved content into the database.
the main and  most important action in this project is that, for each URL there will be a
specific entitled folder created, which will initiate include the text files;
queue.txt and crawled.txt, while queue.txt writes the url of the sites, crawled.txt collects
the content from specific url location.